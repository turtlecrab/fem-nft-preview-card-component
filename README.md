# Frontend Mentor - NFT preview card component

![Design preview for the NFT preview card component coding challenge](./design/desktop-preview.jpg)

## Links
Demo: <https://fem-nft-preview-card-component-ten.vercel.app/>

Submission page: <https://www.frontendmentor.io/solutions/nft-preview-card-component-BkhgJCVVq>

## Notes
Tried to do this challenge as quickly as possible, it took me about 3 hours. At least half an hour of which I tried to align ```li```'s vertically relative to their image bullets. It would be easily done if I had rewritten it with just ```img``` and flex, but I tried to make it work with just ```li``` and failed. Kinda lesson learned, though I still don't know how to do it :)